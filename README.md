# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
### Project tools version

React version: `18.1.0` 

Node version:  `v14.18.1`

### Approach
I have breakdown the project into smaller tasks and build the mvp version of the project that simply have the expected grid then in next phase make it compatible for mobile.
I have used React modal video to show the youtube video on pop up.
I have added snapshot testing to have a test coverage for our application and made sure my refactoring is not breaking the functionality.
If I had more time, I would make it fully responsive and also add more unit test around some of the functionality of it on top of the snapshot testing.
I have created an Html page based on the coding-challenge-mock-up.jpg design using the assets available in this zip file.
When we clicked on the "Learn More" button, it will open a modal and plays a youtube video.
This application is responsive for mobile and screen view. 
