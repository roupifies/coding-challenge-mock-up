import {shallow} from 'enzyme';
import expect from '../../services/chai';
import Item from "./Item";

describe('Item', () => {
    test('should match snapshot', () => {
        expect(shallow(<Item/>)).toMatchSnapshot();
    });
});
