import * as React from 'react';
import Text from "../text/Text";
import './Item.css';


type Props = $ReadOnly<{}>;

export default function Item({name, label, text}:Props) : React.MixedElement{

    return(
        <div className="item">
            <img src={name}  alt={name} className="image" />
            <div className="label">
                <label htmlFor={name}>{label}</label>
            </div>
            <Text text={text}></Text>
        </div>

    );
}