import {shallow} from 'enzyme';
import expect from '../../services/chai';
import ModelYoutube from "./ModelYoutube";


describe('ModelYoutube', () => {
    test('should match snapshot', () => {
        expect(shallow(<ModelYoutube/>)).toMatchSnapshot();
    });
});

describe('render the ModelYoutube component', function() {
    describe('Load Button to show ', function () {

        it('should render the button', function () {
            const modelView = shallow(<ModelYoutube></ModelYoutube>);
            const loadButton = modelView.find('button');

            expect(loadButton).to.have.length(1);
        });

        it('should render the ModelYoutube', function () {
            const modelView = shallow(<ModelYoutube></ModelYoutube>);
            const modalVideo = modelView.find('ModalVideo');

            expect(modalVideo).to.have.length(1);
        });
    });
});


