import * as React from 'react';
import {useState, Fragment} from 'react';
import ModalVideo from "react-modal-video";
import './ModelYoutube.css'

export default function ModelYoutube() {
    const [isOpen, setOpen] = useState(false);
    return(
        <Fragment>
            <ModalVideo channel='youtube' autoplay isOpen={isOpen} videoId="OQ1CwPhE8KQ" onClose={() => setOpen(false)} />
            <button className="btn" onClick={()=> setOpen(true)}>Learn More</button>
        </Fragment>
    );
}