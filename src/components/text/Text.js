import * as React from 'react';

type Props = $ReadOnly<{}>;

export default function Text({text}:Props) : React.MixedElement{

    return(
        <div className="paragraph">
            {text}
        </div>
    );

}