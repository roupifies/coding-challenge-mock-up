import chai from 'chai';
import chaiEnzyme from 'chai-enzyme';

chai.use(chaiEnzyme());

const originalNot = Object.getOwnPropertyDescriptor(chai.Assertion.prototype, 'not').get;

Object.defineProperty(chai.Assertion.prototype, 'not', {
    get() {
        Object.assign(this, this.assignedNot);

        return originalNot.apply(this);
    },
    set(newNot) {
        this.assignedNot = newNot;

        return newNot;
    },
});

const jestExpect = global.expect;
const expect = (actual) => {
    const originalMatchers = jestExpect(actual);
    const chaiMatchers = chai.expect(actual);
    return Object.assign(chaiMatchers, originalMatchers);
};

Object.keys(jestExpect).forEach((key) => {
    expect[key] = jestExpect[key];
});

export default expect;

