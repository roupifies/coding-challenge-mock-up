import {shallow} from 'enzyme';
import expect from './services/chai';
import App from './App';

describe('App', () => {
  test('should match snapshot', () => {
    expect(shallow(<App/>)).toMatchSnapshot();
  });
});
