const helpers = {
    callOfDutyText: function(){
        return "Experience call of Duty: the world's bets-selling video game franchise. Discover the latest updates to this first-person shooter series all in one place, including the latest: Warzone and Black Ops Cold War Season2."
    },
    crushText: function(){
        return "The Devious Vvillais Neo Cortex and Dr.N.Tropy have finally escaped their interdimensional prison, leaving and evil scientist sized hole in the universe."
    },
    headerText: function(){
        return "The World's first independent developer and distributor of video console games & one of the largest third party video game publisher in the world."
    }
}

export default helpers;