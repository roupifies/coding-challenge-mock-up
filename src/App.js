import callOfDuty from './resources/call-of-duty.jpg';
import crash from './resources/crash-bandicoot-4.jpg';
import helpers from './helpers/Helpers'
import './App.css';
import './components/modelVideo/Modal.css';
import Item from "./components/item/Item";
import Text from "./components/text/Text";
import ModelYoutube from "./components/modelVideo/ModelYoutube";

function App() {
  return (
    <div className="main">
        <div className="header">
            <div className="headerText">
            <Text text={helpers.headerText()} ></Text>
            </div>
        </div>
        <ModelYoutube></ModelYoutube>
        <div className="content">
            <Item name={callOfDuty} label={"Call Of Duty"} text={helpers.callOfDutyText()}></Item>
            <Item name={crash} label={"Crush Bandicoot 4"} text={helpers.crushText()}></Item>
        </div>
    </div>
  );
}

export default App;
